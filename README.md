# Webcine

A media center application build as a webapplication (like plex and netflix) written in python 3. 

## Features

* Full html5 based interface (no video player plugins)
* Ahead of time video transcoding for incompatible file formats
* Sickbeard integration

## Roadmap

* Transmission / Sabnzbd / Couchpotato integration
* Mobile version
* Android TV version
* Theme support

## Requirements

* mysql server
* webserver to handle static file serving and wsgi (apache, nginx)
* Sickbeard if you want to add a tv library (It depends on sickbeard metadata)

## Screenshots

![Dashboard](http://brixitcdn.net/github/webcine/dashboard.png)
![Player](http://brixitcdn.net/github/webcine/player.png)

## Installation

```bash
$ git clone [this repo] /opt/webcine
$ apt install apache2 libapache2-mod-wsgi-py3 mysql-server
$ a2enmod wsgi

# This is for debian 9, flask_peewee is packaged in debian 10
$ apt install python3-pip python3-pymysql python3-tvdb-api python3-tmdbsimple python3-xmltodict
$ pip3 install flask_peewee python-transcoded

# Copy the configuration template
$ cp /opt/webcine/config.example.ini /etc/webcine.conf
```

Configure mod_wsgi in apache (see `installation/apache-vhost.conf` as example)
Also create an appropiate configuration for `python-transcoded` as described in the
[documentation for transcoded](https://python-transcoded.readthedocs.io/en/latest/).

In the `transcoded` config the `path` setting should be the same as the `storage` setting
in webcine and the callback should be set to `http://url-to-your-webcine/transcode-callback`

Open the url defined in your apache vhost and login with admin/admin